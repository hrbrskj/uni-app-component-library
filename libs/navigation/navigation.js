/**
 * 页面展示列表数据
 */
export default {
	data: [{
			title: '开源部分',
			backgroundColor: 'tn-cool-bg-color-1',
			list: [{
					icon: 'code',
					title: '社交发帖',
					url: '/pages/from/edit'
				},

				{
					icon: 'code',
					title: '申请发票',
					url: '/pages/from/invoice'
				},
				{
					icon: 'code',
					title: '上传文件',
					url: '/pages/from/upload'
				},
				{
					icon: 'code',
					title: '申请报修',
					url: '/pages/from/report'
				},
				{
					icon: 'code',
					title: '问卷详情',
					url: '/pages/from/questionnaire'
				},
				{
					icon: 'code',
					title: '在线做题',
					url: '/pages/from/kaoshi'
				},
				{
					icon: 'code',
					title: '火箭用户登录',
					url: '/pages/from/login1'
				},
				{
					icon: 'code',
					title: '设备申请',
					url: '/pages/from/device'
				},
				{
					icon: 'code',
					title: '申请报销',
					url: '/pages/from/cost'
				},
				{
					icon: 'code',
					title: '录入客户信息',
					url: '/pages/from/new_customer'
				},
				{
					icon: 'code',
					title: '卡片列表',
					url: '/pages/information/tabulation'
				},
				{
					icon: 'code',
					title: '工作台',
					url: '/pages/information/staging'
				},
				{
					icon: 'code',
					title: '待办事项',
					url: '/pages/information/pending'
				},
				{
					icon: 'code',
					title: '审批进程',
					url: '/pages/information/approval'
				},
				{
                    icon: 'code',
                    title: '底部导航2',
                    url: '/pages/tabbar/index1'
                }
			]
		},
//获取代码可以联系我们，微信：yunchen_2023
		{
			title: '会员专享',
			backgroundColor: 'tn-cool-bg-color-1',
			list: [{
                   			icon: 'code',
                   			title: '下拉式筛选菜单',
                   			url: '/pages/waterfall/screen'
                   		},{
                   			icon: 'code',
                   			title: '多条件排序',
                   			url: '/pages/waterfall/sort'
                   		},
                   		{
                   			icon: 'code',
                   			title: '日历',
                   			url: '/pages/waterfall/calendar'
                   		},

                   		{
                   			icon: 'code',
                   			title: '社交发帖',
                   			url: '/pages/from/edit'
                   		},
                   		{
                   			icon: 'code',
                   			title: '身份认证',
                   			url: '/pages/from/certification'
                   		},
                   		{
                   			icon: 'code',
                   			title: '申请发票',
                   			url: '/pages/from/invoice'
                   		},
                   		{
                   			icon: 'code',
                   			title: '上传文件',
                   			url: '/pages/from/upload'
                   		},
                   		{
                   			icon: 'code',
                   			title: '申请报修',
                   			url: '/pages/from/report'
                   		},
                   		{
                   			icon: 'code',
                   			title: '问卷详情',
                   			url: '/pages/from/questionnaire'
                   		},
                   		{
                   			icon: 'code',
                   			title: '在线做题',
                   			url: '/pages/from/kaoshi'
                   		},
                   		{
                   			icon: 'code',
                   			title: '用户登录',
                   			url: '/pages/from/login'
                   		}, {
                   			icon: 'code',
                   			title: '火箭用户登录',
                   			url: '/pages/from/login1'
                   		},
                   		{
                   			icon: 'code',
                   			title: '设备申请',
                   			url: '/pages/from/device'
                   		},
                   		{
                   			icon: 'code',
                   			title: '签名面板',
                   			url: '/pages/from/signature'
                   		},
                   		{
                   			icon: 'code',
                   			title: '申请报销',
                   			url: '/pages/from/cost'
                   		},
                   		{
                   			icon: 'code',
                   			title: '录入客户信息',
                   			url: '/pages/from/new_customer'
                   		}, {
                   			icon: 'code',
                   			title: '社交类型',
                   			url: '/pages/waterfall/shejiao'
                   		},
                   		{
                   			icon: 'code',
                   			title: '商品分类类型',
                   			url: '/pages/waterfall/goods'
                   		},
                   		{
                   			icon: 'code',
                   			title: '商品列表',
                   			url: '/pages/waterfall/preferred'
                   		},
                   		{
                   			icon: 'code',
                   			title: '硬件列表',
                   			url: '/pages/waterfall/apparatus'
                   		}, {
                   			icon: 'code',
                   			title: '资讯列表',
                   			url: '/pages/information/news'
                   		},
                   		{
                   			icon: 'code',
                   			title: '卡片列表',
                   			url: '/pages/information/tabulation'
                   		},
                   		{
                   			icon: 'code',
                   			title: '工作台',
                   			url: '/pages/information/staging'
                   		},
                   		{
                   			icon: 'code',
                   			title: '图表示例',
                   			url: '/pages/information/charts'
                   		},
                   		{
                   			icon: 'code',
                   			title: '消息通知',
                   			url: '/pages/information/message'
                   		},
                   		{
                   			icon: 'code',
                   			title: '滑动菜单',
                   			url: '/pages/information/swipe'
                   		},
                   		{
                   			icon: 'code',
                   			title: '报价结果',
                   			url: '/pages/information/quotation'
                   		},
                   		{
                   			icon: 'code',
                   			title: '物流轨迹',
                   			url: '/pages/information/trajectory'
                   		},
                   		{
                   			icon: 'code',
                   			title: '待办事项',
                   			url: '/pages/information/pending'
                   		},
                   		{
                   			icon: 'code',
                   			title: '审批进程',
                   			url: '/pages/information/approval'
                   		},
                   		{
                   			icon: 'code',
                   			title: '仪表盘',
                   			url: '/pages/information/gauge'
                   		},
                   		{
                   			icon: 'code',
                   			title: '树形菜单',
                   			url: '/pages/information/treemenu'
                   		},
                   		{
                   			icon: 'code',
                   			title: '短剧播放示例',
                   			url: '/pages/information/video'
                   		},
                   		{
                   			icon: 'code',
                   			title: '长按拖拽排序',
                   			url: '/pages/information/list'
                   		}, {
                   			icon: 'code',
                   			title: '电影在线选座',
                   			url: '/pages/information/selection'
                   		},
                   		{
                   			icon: 'code',
                   			title: '在线表格',
                   			url: '/pages/information/table'
                   		},
                   		{
                   			icon: 'code',
                   			title: '纵向表格',
                   			url: '/pages/information/verticaltable'
                   		}, {
                   			icon: 'code',
                   			title: '底部导航1',
                   			url: '/pages/tabbar/index'
                   		},
                   		{
                   			icon: 'code',
                   			title: '底部导航2',
                   			url: '/pages/tabbar/index1'
                   		},
                   		{
                   			icon: 'code',
                   			title: '底部导航3',
                   			url: '/pages/tabbar/index2'
                   		},
                   		{
                   			icon: 'code',
                   			title: '底部导航4',
                   			url: '/pages/tabbar/index3'
                   		},
                   		{
                   			icon: 'code',
                   			title: '底部导航5',
                   			url: '/pages/tabbar/index4'
                   		},
                   		{
                   			icon: 'code',
                   			title: '底部导航6',
                   			url: '/pages/tabbar/index5'
                   		}, {
                   			icon: 'code',
                   			title: '大转盘抽奖',
                   			url: '/pages/other/wheel'
                   		},
                   		{
                   			icon: 'code',
                   			title: '九宫格抽奖',
                   			url: '/pages/other/squared'
                   		},
                   		{
                   			icon: 'code',
                   			title: '老虎机抽奖',
                   			url: '/pages/other/slots'
                   		},
                   		{
                   			icon: 'code',
                   			title: '砸金蛋活动',
                   			url: '/pages/other/golden'
                   		},

                   		{
                   			icon: 'code',
                   			title: '排行榜',
                   			url: '/pages/other/king'
                   		},
                   		{
                   			icon: 'code',
                   			title: '在线投票',
                   			url: '/pages/other/toupiao'
                   		},
                   		{
                   			icon: 'code',
                   			title: '旋转特效',
                   			url: '/pages/other/jiuhe'
                   		},
                   		{
                   			icon: 'code',
                   			title: '地图路线',
                   			url: '/pages/other/trajectory'
                   		},
                   		{
                   			icon: 'code',
                   			title: '地图地址显示',
                   			url: '/pages/other/map'
                   		},
                   		{
                   			icon: 'code',
                   			title: '客户画像',
                   			url: '/pages/other/portrait'
                   		},
                   		{
                   			icon: 'code',
                   			title: '流行特效',
                   			url: '/pages/other/suspended'
                   		},
                   		{
                   			icon: 'code',
                   			title: '全屏轮播特效',
                   			url: '/pages/other/fullpage'
                   		},
                   		{
                   			icon: 'code',
                   			title: '充值页面',
                   			url: '/pages/other/money'
                   		},
                   		{
                   			icon: 'code',
                   			title: '兑换详情页',
                   			url: '/pages/other/gift'
                   		},
                   		{
                   			icon: 'code',
                   			title: '可视化页面',
                   			url: '/pages/other/visualization'
                   		},
                   		{
                   			icon: 'code',
                   			title: '图片剪裁',
                   			url: '/pages/other/cropper'
                   		}


			],
		}


	]
}